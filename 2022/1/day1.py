import pandas as pd
import numpy as np
f = open('input.txt', 'r')
i = open('output.txt', 'w')
i.write(f.read().replace('\n\n', '\r').replace('\n', ','))
i.close
f.close

df = pd.read_fwf('output.txt', header=None)
df = df[0].str.split(',', expand=True)
df = df.convert_dtypes(infer_objects=False, convert_integer=True)
df = df.apply(pd.to_numeric, errors='coerce').fillna(value=0)
totals = df.sum(axis=1).sort_values()
totals = totals.nlargest(n=3)
# list(totals.columns)
print(totals.sum())
