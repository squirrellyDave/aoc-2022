totalScore = 0


def winLose(opponent, me):
    if (me == 'X' and opponent == 'C') or \
            (me == 'Y' and opponent == 'A') or \
            (me == 'Z' and opponent == 'B'):
        return 6
    elif (me == 'X' and opponent == 'A') or \
            (me == 'Y' and opponent == 'B') or \
            (me == 'Z' and opponent == 'C'):
        return 3
    else:
        return 0


def calc(val):
    if val == 'X':
        return 1
    elif val == 'Y':
        return 2
    elif val == 'Z':
        return 3


def part2(opp, me):
    if me == 'X':
        if opp == 'A':
            return 3
        elif opp == 'B':
            return 1
        else:
            return 2
    elif me == 'Y':
        if opp == 'A':
            return 4
        elif opp == 'B':
            return 5
        else:
            return 6
    else:
        if opp == 'A':
            return 8
        elif opp == 'B':
            return 9
        else:
            return 7


with open('data.txt') as f:
    for line in f:
        opp = line.split()[0]
        me = line.split()[1]
        rndScore = part2(opp, me)
        # rndScore = winLose(opp, me) + calc(me)
        totalScore += rndScore
        print(rndScore)

print(totalScore)
