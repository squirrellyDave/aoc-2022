import string


alphabet = string.ascii_letters
score = 0


def findMatch(front, rear):
    for i, v in enumerate(front):
        for idx, char in enumerate(rear):
            if char == v:
                for ind, letter in enumerate(alphabet):
                    if v == letter:
                        return ind + 1


def getVal(v):
    for ind, letter in enumerate(alphabet):
        if v == letter:
            return ind + 1


with open('sample.txt') as f:
    lines = f.readlines()
    for i in range(0, len(lines), 3):
        s = list(set(lines[i]) & set(lines[i + 1]) & set(lines[i + 2]))
        s.remove('\n')
        score += getVal(s[0])
        print(score)
# with open('sample.txt')as file:
#     for line in file:
#         print(score)
#         split = int((len(line) - 1) / 2)
#         front, rear = line[:split], line[split:]
#         score += findMatch(front, rear)
print(score)
